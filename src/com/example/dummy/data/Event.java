package com.example.dummy.data;

public class Event {

	private int image;
	private String nama;
	private String tanggal;

	public Event(int image, String nama, String tanggal) {
		this.image = image;
		this.nama = nama;
		this.tanggal = tanggal;
	}

	public int getImage() {
		return image;
	}

	public String getNama() {
		return nama;
	}

	public String getTanggal() {
		return tanggal;
	}

}
