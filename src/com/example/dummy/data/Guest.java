package com.example.dummy.data;

public class Guest {

	private int image;
	private String nama;

	public Guest(int image, String nama) {
		this.image = image;
		this.nama = nama;
	}

	public int getImage() {
		return image;
	}

	public String getNama() {
		return nama;
	}

}
