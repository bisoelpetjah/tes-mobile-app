package com.example.dummy.adapter;

import java.util.List;

import com.example.dummy.R;
import com.example.dummy.data.Event;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EventListAdapter extends BaseAdapter {

	private Context context;
	private List<Event> events;

	public EventListAdapter(Context context, List<Event> events) {
		this.context = context;
		this.events = events;
	}

	@Override
	public int getCount() {
		return events.size();
	}

	@Override
	public Event getItem(int position) {
		return events.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.list_event_item, null);
		}
		ImageView image = (ImageView) convertView.findViewById(R.id.imageEventImage);
		image.setImageResource(getItem(position).getImage());
		TextView nama = (TextView) convertView.findViewById(R.id.textEventNama);
		nama.setText(getItem(position).getNama());
		TextView tanggal = (TextView) convertView.findViewById(R.id.textEventTanggal);
		tanggal.setText(getItem(position).getTanggal());
		return convertView;
	}

}
