package com.example.dummy;

import java.util.LinkedList;
import java.util.List;

import com.example.dummy.adapter.EventListAdapter;
import com.example.dummy.data.Event;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;

public class EventActivity extends Activity {

	EventListAdapter listAdapter;
	ListView listEvent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);
		List<Event> dummy = new LinkedList<Event>();
		dummy.add(new Event(R.drawable.ic_launcher, "Event 1", "1 Januari 2001"));
		dummy.add(new Event(R.drawable.ic_launcher, "Event 2", "2 Februari 2002"));
		dummy.add(new Event(R.drawable.ic_launcher, "Event 3", "3 Maret 2003"));
		dummy.add(new Event(R.drawable.ic_launcher, "Event 4", "4 April 2004"));
		dummy.add(new Event(R.drawable.ic_launcher, "Event 5", "5 Mei 2005"));
		listAdapter = new EventListAdapter(this, dummy);
		listEvent = (ListView) findViewById(R.id.listEvent);
		listEvent.setAdapter(listAdapter);
		listEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent result = new Intent();
				result.putExtra("nama", listAdapter.getItem(position).getNama());
				setResult(RESULT_OK, result);
				finish();
			}
		});
		
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
