package com.example.dummy;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;

public class PilihActivity extends Activity {

	public static final int EVENT_REQUEST_CODE = 111;
	public static final int GUEST_REQUEST_CODE = 222;

	TextView textNama;
	Button buttonPilihEvent;
	Button buttonPilihGuest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pilih);
		textNama = (TextView) findViewById(R.id.textNama);
		buttonPilihEvent = (Button) findViewById(R.id.buttonPilihEvent);
		buttonPilihEvent.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(PilihActivity.this, EventActivity.class);
				startActivityForResult(intent, EVENT_REQUEST_CODE);
			}
		});
		buttonPilihGuest = (Button) findViewById(R.id.buttonPilihGuest);
		buttonPilihGuest.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(PilihActivity.this, GuestActivity.class);
				startActivityForResult(intent, GUEST_REQUEST_CODE);
			}
		});
		// Show the Up button in the action bar.
		setupActionBar();
		textNama.setText(getIntent().getStringExtra("nama"));
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pilih, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("test", "result");
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case EVENT_REQUEST_CODE: {
				buttonPilihEvent.setText(data.getStringExtra("nama"));
				break;
			}
			case GUEST_REQUEST_CODE: {
				buttonPilihGuest.setText(data.getStringExtra("nama"));
				break;
			}
			}
		}
	}

}
