package com.example.dummy.adapter;

import java.util.List;

import com.example.dummy.R;
import com.example.dummy.data.Guest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GuestGridAdapter extends BaseAdapter {

	private Context context;
	private List<Guest> guests;

	public GuestGridAdapter(Context context, List<Guest> guests) {
		super();
		this.context = context;
		this.guests = guests;
	}

	@Override
	public int getCount() {
		return guests.size();
	}

	@Override
	public Guest getItem(int position) {
		return guests.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.grid_guest_item, null);
		}
		ImageView image = (ImageView) convertView.findViewById(R.id.imageGuestImage);
		image.setImageResource(getItem(position).getImage());
		TextView nama = (TextView) convertView.findViewById(R.id.textGuestNama);
		nama.setText(getItem(position).getNama());
		return convertView;
	}

}
